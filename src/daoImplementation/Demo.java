package daoImplementation;

import daoImplementation.DAOs.*;
import daoImplementation.entities.*;
import javafx.geometry.Pos;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {

        //Employees employees = new Employees(4, "Marian", "Kowalski", 22);
        EmployeesDAO dao = new EmployeesDAO();
        //dao.insert(employees);
        List<Employees> emp = dao.select();
        for (Employees employees : emp) {
            System.out.println(employees);
        }

        //Jobs job = new Jobs(4,"welder", 4500);
        JobsDAO jobsDAO = new JobsDAO();
        //jobsDAO.insert(job);
        List<Jobs> jobs = jobsDAO.select();
        for (Jobs job : jobs) {
            System.out.println(job);
        }

        //Bonus bonus = new Bonus(4, "Christmas bonus", 1000);
        BonusDAO bonusDAO = new BonusDAO();
        //bonusDAO.insert(bonus);

        List<Bonus> bonuses = bonusDAO.select();
        for (Bonus bonus : bonuses) {
            System.out.println(bonus);
        }

        //Adress adress = new Adress(4, "Wroclaw", "Robotnicza", 22);
        AdressDAO adressDAO = new AdressDAO();
        //adressDAO.insert(adress);

        List<Adress> adresses = adressDAO.select();
        for (Adress adress : adresses) {
            System.out.println(adress);
        }

        // Post post = new Post(4, "manager", 4);
        PostDAO postDAO = new PostDAO();
        //postDAO.insert(post);
        List<Post> posts = postDAO.select();
        for (Post post : posts) {
            System.out.println(post);
        }
    }
}
