package daoImplementation.entities;

public class Bonus implements Entity{
    private int id;
    private String bonusName;
    private int bonusValue;

    public Bonus(int id, String bonusName, int bonusValue) {
        this.id = id;
        this.bonusName = bonusName;
        this.bonusValue = bonusValue;
    }

    public Bonus() {
    }

    public int getId() {
        return id;
    }

    public String getBonusName() {
        return bonusName;
    }

    public int getBonusValue() {
        return bonusValue;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBonusName(String bonusName) {
        this.bonusName = bonusName;
    }

    public void setBonusValue(int bonusValue) {
        this.bonusValue = bonusValue;
    }

    @Override
    public String toString() {
        return "Bonus{" +
                "id=" + id +
                ", bonusName='" + bonusName + '\'' +
                ", bonusValue=" + bonusValue +
                '}';
    }
}
