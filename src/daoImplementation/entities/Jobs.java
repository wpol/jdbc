package daoImplementation.entities;

public class Jobs implements Entity{
    private int id;
    private String jobname;
    private int minsalary;

    public Jobs(int id, String jobname, int minsalary) {
        this.id = id;
        this.jobname = jobname;
        this.minsalary = minsalary;
    }

    public Jobs() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJobname(String jobname) {
        this.jobname = jobname;
    }

    public void setMinsalary(int minsalary) {
        this.minsalary = minsalary;
    }

    public int getId() {
        return id;
    }

    public String getJobname() {
        return jobname;
    }

    public int getMinsalary() {
        return minsalary;
    }

    @Override
    public String toString() {
        return "Jobs{" +
                "id=" + id +
                ", jobname='" + jobname + '\'' +
                ", minsalary=" + minsalary +
                '}';
    }
}
