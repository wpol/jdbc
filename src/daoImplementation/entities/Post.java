package daoImplementation.entities;

public class Post implements Entity{
    private int id;
    private String postname;
    private int paymentcategory;

    public Post(int id, String postname, int paymentcategory) {
        this.id = id;
        this.postname = postname;
        this.paymentcategory = paymentcategory;
    }

    public Post() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public void setPaymentcategory(int paymentcategory) {
        this.paymentcategory = paymentcategory;
    }

    public int getId() {
        return id;
    }

    public String getPostname() {
        return postname;
    }

    public int getPaymentcategory() {
        return paymentcategory;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", postname='" + postname + '\'' +
                ", paymentcategory=" + paymentcategory +
                '}';
    }
}
