package daoImplementation.entities;

public class Adress implements Entity{
    private int id;
    private String city;
    private String street;
    private int streetNumber;

    public Adress(int id, String city, String street, int streetNumber) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
    }

    public Adress() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", streetNumber=" + streetNumber +
                '}';
    }
}
