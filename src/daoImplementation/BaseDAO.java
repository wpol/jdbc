package daoImplementation;

import daoImplementation.entities.Entity;

import java.sql.*;
import java.util.List;

public abstract class BaseDAO<T extends Entity> {
    //dane do polaczenia i wybor sterownika
    //private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/EMP";

    //login i haslo do bazy
    private static final String USER = "root";
    private static final String PASS = "";

    protected String sql;

    public abstract void insert(T toInsert);

    public abstract List<T> select();

    public abstract void update(T toUpdate);

    public abstract void delete(T toDelete);

    protected abstract List<T> parse(ResultSet rs);

    protected void execute() {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            stmt.execute(sql);

            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected List<T> executeSelect() {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();

            ResultSet resultSet = stmt.executeQuery(sql);
            List<T> result = parse(resultSet);

            resultSet.close();
            stmt.close();
            conn.close();
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
