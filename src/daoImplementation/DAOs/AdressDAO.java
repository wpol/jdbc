package daoImplementation.DAOs;

import daoImplementation.BaseDAO;
import daoImplementation.SQLGenerators.AdressSQLGenerator;
import daoImplementation.entities.Adress;
import daoImplementation.entities.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdressDAO extends BaseDAO<Adress> {
    AdressSQLGenerator adressSQLGenerator = new AdressSQLGenerator();

    @Override
    public void insert(Adress toInsert) {
        sql = adressSQLGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Adress> select() {
        sql = adressSQLGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Adress toUpdate) {
        sql = adressSQLGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Adress toDelete) {
        sql = adressSQLGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Adress> parse(ResultSet rs) {
        List<Adress> result = new ArrayList<>();
        try{
            while (rs.next()){
                int id = rs.getInt("id");
                String city = rs.getString("city");
                String street = rs.getString("street");
                int streetnumber = rs.getInt("streetnumber");

                result.add(new Adress(id, city, street, streetnumber));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }
}
