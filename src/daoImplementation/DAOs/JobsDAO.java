package daoImplementation.DAOs;

import daoImplementation.BaseDAO;
import daoImplementation.SQLGenerators.JobsSQLGenerator;
import daoImplementation.entities.Jobs;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobsDAO extends BaseDAO<Jobs> {
    JobsSQLGenerator jobsSQLGenerator = new JobsSQLGenerator();

    @Override
    public void insert(Jobs toInsert) {
        sql = jobsSQLGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Jobs> select() {
        sql = jobsSQLGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Jobs toUpdate) {
        sql = jobsSQLGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Jobs toDelete) {
        sql = jobsSQLGenerator.delete(toDelete);
        execute();

    }

    @Override
    protected List<Jobs> parse(ResultSet rs) {
        List<Jobs> result = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String jobName = rs.getString("jobname");
                int minsalary = rs.getInt("minsalary");

                result.add(new Jobs(id, jobName, minsalary));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
