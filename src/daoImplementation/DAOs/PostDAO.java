package daoImplementation.DAOs;

import daoImplementation.BaseDAO;
import daoImplementation.SQLGenerators.PostSQLGenerator;
import daoImplementation.entities.Post;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostDAO extends BaseDAO<Post> {
    PostSQLGenerator postSQLGenerator = new PostSQLGenerator();
    @Override
    public void insert(Post toInsert) {
        sql = postSQLGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Post> select() {
        sql = postSQLGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Post toUpdate) {
        sql = postSQLGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Post toDelete) {
        sql = postSQLGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Post> parse(ResultSet rs) {
        List<Post> result = new ArrayList<>();
        try{
            while (rs.next()){
                int id = rs.getInt("id");
                String postname = rs.getString("postname");
                int paymentcategory = rs.getInt("paymentcategory");

                result.add(new Post(id, postname, paymentcategory));
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}
