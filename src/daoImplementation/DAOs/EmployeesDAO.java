package daoImplementation.DAOs;

import daoImplementation.BaseDAO;
import daoImplementation.SQLGenerators.EmployeesSQLGenerator;
import daoImplementation.entities.Employees;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeesDAO extends BaseDAO<Employees> {
    EmployeesSQLGenerator sqlGenerator = new EmployeesSQLGenerator();

    @Override
    public void insert(Employees toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Employees> select() {
        sql = sqlGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Employees toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Employees toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Employees> parse(ResultSet rs) {
        ArrayList<Employees> result = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastname");
                int age = rs.getInt("age");

                result.add(new Employees(id, firstName, lastName, age));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
