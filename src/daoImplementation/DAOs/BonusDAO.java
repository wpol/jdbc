package daoImplementation.DAOs;

import daoImplementation.BaseDAO;
import daoImplementation.SQLGenerators.BonusSQLGenerator;
import daoImplementation.entities.Bonus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BonusDAO extends BaseDAO<Bonus> {
    BonusSQLGenerator bonusSQLGenerator = new BonusSQLGenerator();

    @Override
    public void insert(Bonus toInsert) {
        sql = bonusSQLGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Bonus> select() {
        sql = bonusSQLGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Bonus toUpdate) {
        sql = bonusSQLGenerator.update(toUpdate);
        execute();

    }

    @Override
    public void delete(Bonus toDelete) {
        sql = bonusSQLGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Bonus> parse(ResultSet rs) {
        List<Bonus> result = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String bonusName = rs.getString("bonusname");
                int bonusValue = rs.getInt("bonusvalue");

                result.add(new Bonus(id, bonusName, bonusValue));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
