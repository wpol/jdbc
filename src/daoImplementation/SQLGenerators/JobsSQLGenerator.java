package daoImplementation.SQLGenerators;

import daoImplementation.entities.Jobs;

public class JobsSQLGenerator implements SQLGenerator<Jobs> {

    @Override
    public String insert(Jobs toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into Jobs ")
                .append("(id, jobname, minsalary) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getJobname()).append("'")
                .append(", ").append(toInsert.getMinsalary()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM jobs;";
    }

    @Override
    public String update(Jobs toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE jobs SET jobname='")
                .append(toUpdate.getJobname()).append("',")
                .append("minsalary=").append(toUpdate.getMinsalary())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Jobs toDelete) {
        return "DELETE FROM jobs WHERE id = " + toDelete.getId() + ";";
    }
}
