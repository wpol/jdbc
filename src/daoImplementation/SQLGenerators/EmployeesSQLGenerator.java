package daoImplementation.SQLGenerators;

import daoImplementation.entities.Employees;

public class EmployeesSQLGenerator implements SQLGenerator<Employees> {
    @Override
    public String insert(Employees toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into Employees ")
                .append("(id, firstname, lastname, age) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getFirstName()).append("'")
                .append(", '").append(toInsert.getLastName()).append("'")
                .append(", ").append(toInsert.getAge()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM Employees;";
    }

    @Override
    public String update(Employees toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE Employees SET firstname='")
                .append(toUpdate.getFirstName()).append("',")
                .append("lastname='").append(toUpdate.getLastName()).append("',")
                .append("age=").append(toUpdate.getAge())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Employees toDelete) {
        return "DELETE FROM Employees WHERE id = " + toDelete.getId() + ";";
    }
}
