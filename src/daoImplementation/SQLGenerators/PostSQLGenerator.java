package daoImplementation.SQLGenerators;

import daoImplementation.entities.Post;

public class PostSQLGenerator implements SQLGenerator<Post> {

    @Override
    public String insert(Post toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into post ")
                .append("(id, postname, paymentcategory) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getPostname()).append("'")
                .append(", ").append(toInsert.getPaymentcategory()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM post;";
    }

    @Override
    public String update(Post toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE post SET postname='")
                .append(toUpdate.getPostname()).append("',")
                .append("paymentcategory=").append(toUpdate.getPaymentcategory())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Post toDelete) {
        return "DELETE FROM post WHERE id =" + toDelete.getId() + ";";
    }
}
