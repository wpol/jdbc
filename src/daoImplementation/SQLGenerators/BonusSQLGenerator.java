package daoImplementation.SQLGenerators;

import daoImplementation.entities.Bonus;

public class BonusSQLGenerator implements SQLGenerator<Bonus> {

    @Override
    public String insert(Bonus toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into bonus ")
                .append("(id, bonusname, bonusvalue) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getBonusName()).append("'")
                .append(", ").append(toInsert.getBonusValue()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM bonus;";
    }

    @Override
    public String update(Bonus toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE bonus SET bonusname='")
                .append(toUpdate.getBonusName()).append("',")
                .append("bonusvalue=").append(toUpdate.getBonusValue())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Bonus toDelete) {
        return "DELETE FROM bonus WHERE id = " + toDelete.getId() + ";";
    }
}
