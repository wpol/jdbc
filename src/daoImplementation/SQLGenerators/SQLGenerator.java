package daoImplementation.SQLGenerators;

import daoImplementation.entities.Entity;

public interface SQLGenerator<T extends Entity> {
    String insert(T toInsert);
    String selectAll();
    String update(T toUpdate);
    String delete(T toDelete);

}