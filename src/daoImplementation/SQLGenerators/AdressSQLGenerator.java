package daoImplementation.SQLGenerators;

import daoImplementation.entities.Adress;
import daoImplementation.entities.Employees;

public class AdressSQLGenerator implements SQLGenerator<Adress> {
    @Override
    public String insert(Adress toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into adress ")
                .append("(id, city, street, streetnumber) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getCity()).append("'")
                .append(", '").append(toInsert.getStreet()).append("'")
                .append(", ").append(toInsert.getStreetNumber()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "SELECT * FROM adress;";
    }

    @Override
    public String update(Adress toUpdate) {
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE adress SET city='")
                    .append(toUpdate.getCity()).append("',")
                    .append("street='").append(toUpdate.getStreet()).append("',")
                    .append("streetnumber=").append(toUpdate.getStreetNumber())
                    .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Adress toDelete) {
        return "DELETE FROM adress WHERE id = " + toDelete.getId() + ";";
    }
}
