import java.sql.*;

public class Main {
    //dane do polaczenia i wybor sterownika
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/EMP";

    //login i haslo do bazy
    private static final String USER = "root";
    private static final String PASS = "";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            //1.Rejestrowanie sterownika
            //Class.forName(JDBC_DRIVER); // - potrzebne dla starych projektow!!!

            //2. Nawiazujemy polaczenie
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //3. Tworzymy zapytanie - Statement
            stmt = conn.createStatement();
            String sql = "SELECT * FROM Employees";
            //3.a Wykonanie zapytania
            ResultSet rs = stmt.executeQuery(sql);

            //4. Rozpakowanie danych
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastname");
                int age = rs.getInt("age");

                System.out.printf("Id: %d, Imie: %s, Nazwisko: %s, Wiek: %d\n",
                        id, firstName, lastName, age);
            }
            //5. Sprzatenie - zamkniecie RS
            rs.close();

            //6. Sprzatanie - zamykanie statement
            stmt.close();

            //7. Sprzatenie - zamykanie polaczenia
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
