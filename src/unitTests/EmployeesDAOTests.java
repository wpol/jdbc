package unitTests;

import daoImplementation.DAOs.*;
import daoImplementation.entities.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class EmployeesDAOTests {

    @Test
    public void insertEmployeesTest() throws Exception {
        Employees employees = new Employees(999, "AAA", "BBB", 1);
        EmployeesDAO dao = new EmployeesDAO();
        List<Employees> pracownicy = dao.select();
        for (Employees employeesList : pracownicy) {
            if (employeesList.getId() == employees.getId()) {
                throw new Exception("Jest zle");
            }
        }
        dao.insert(employees);
        boolean isOK = false;
        List<Employees> pracownicyPoInsercie = dao.select();
        for (Employees employeesList : pracownicyPoInsercie) {
            if (employeesList.getId() == employees.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);
    }

    @Test
    public void updateEmployeesTest() throws Exception {
        Employees employees = new Employees(999, "DDD", "EEE", 1);
        boolean isOK = false;
        EmployeesDAO dao = new EmployeesDAO();
        List<Employees> pracownicy = dao.select();
        for (Employees employee : pracownicy) {
            if (employee.getId() == employees.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.update(employees);
        boolean exists = false;
        List<Employees> pracownicyPoUpdate = dao.select();
        for (Employees employeesList : pracownicyPoUpdate) {
            if (employeesList.getId() == employees.getId()
                    && employeesList.getFirstName().equals(employees.getFirstName())
                    && employeesList.getLastName().equals(employees.getLastName())
                    && employeesList.getAge() == employees.getAge()) {
                exists = true;
                break;
            }
        }
        Assert.assertTrue(exists);
    }

    @Test
    public void deleteEmployeesTest() throws Exception {
        Employees employees = new Employees(999, "DDD", "EEE", 1);
        boolean isOK = false;
        EmployeesDAO dao = new EmployeesDAO();
        List<Employees> pracownicy = dao.select();
        for (Employees employee : pracownicy) {
            if (employee.getId() == employees.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.delete(employees);
        boolean exists = false;
        List<Employees> pracownicyPo = dao.select();
        for (Employees employeesList : pracownicyPo) {
            if (employeesList.getId() == employees.getId()) {
                exists = true;
            }
        }
        Assert.assertFalse(exists);
    }


    @Test
    public void insertAdressTest() throws Exception {
        Adress adress = new Adress(999, "AAA", "BBB", 1);
        AdressDAO dao = new AdressDAO();
        List<Adress> adresses = dao.select();
        for (Adress adress1 : adresses) {
            if (adress1.getId() == adress.getId()) {
                throw new Exception("Jest zle");
            }
        }
        dao.insert(adress);
        boolean isOK = false;
        List<Adress> adressPoInsercie = dao.select();
        for (Adress adress1 : adressPoInsercie) {
            if (adress1.getId() == adress.getId()) {
                isOK = true;
            }
        }
        Assert.assertTrue(isOK);
    }

    @Test
    public void updateAdressTest() throws Exception {
        Adress adress = new Adress(999, "DDD", "EEE", 1);
        boolean isOK = false;
        AdressDAO dao = new AdressDAO();
        List<Adress> adresses = dao.select();
        for (Adress adress1 : adresses) {
            if (adress1.getId() == adress.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.update(adress);
        boolean exists = false;
        List<Adress> adressPoUpdate = dao.select();
        for (Adress adress1 : adressPoUpdate) {
            if (adress1.getId() == adress.getId() && adress1.getCity().equals(adress.getCity())
                    && adress1.getStreet().equals(adress.getStreet())
                    && adress1.getStreetNumber() == adress.getStreetNumber()) {
                exists = true;
                break;
            }
        }
        Assert.assertTrue(exists);
    }

    @Test
    public void deleteAdressTest() throws Exception {
        Adress adress = new Adress(999, "DDD", "EEE", 1);
        boolean isOK = false;
        AdressDAO dao = new AdressDAO();
        List<Adress> adresses = dao.select();
        for (Adress adress1 : adresses) {
            if (adress1.getId() == adress.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.delete(adress);
        boolean exists = false;
        List<Adress> adressPo = dao.select();
        for (Adress adress1 : adressPo) {
            if (adress1.getId() == adress.getId()) {
                exists = true;
            }
        }
        Assert.assertFalse(exists);
    }

    @Test
    public void insertBonusTest() throws Exception {
        Bonus bonus = new Bonus(999, "AAA", 1000);
        BonusDAO dao = new BonusDAO();
        List<Bonus> bonuses = dao.select();
        for (Bonus bonus1 : bonuses) {
            if (bonus1.getId() == bonus.getId()) {
                throw new Exception("Jest zle");
            }
        }
        dao.insert(bonus);
        boolean isOK = false;
        List<Bonus> bonusPoInsercie = dao.select();
        for (Bonus bonus1 : bonusPoInsercie) {
            if (bonus1.getId() == bonus.getId()) {
                isOK = true;
            }
        }
        Assert.assertTrue(isOK);
    }

    @Test
    public void updateBonusTest() throws Exception {
        Bonus bonus = new Bonus(999, "BBB", 2000);
        boolean isOK = false;
        BonusDAO dao = new BonusDAO();
        List<Bonus> bonuses = dao.select();
        for (Bonus bonus1 : bonuses) {
            if (bonus1.getId() == bonus.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.update(bonus);
        boolean exists = false;
        List<Bonus> bonusPoUpdate = dao.select();
        for (Bonus bonus1 : bonusPoUpdate) {
            if (bonus1.getId() == bonus.getId() && bonus1.getBonusName().equals(bonus.getBonusName())
                    && bonus1.getBonusValue() == bonus.getBonusValue()) {
                exists = true;
                break;
            }
        }
        Assert.assertTrue(exists);
    }

    @Test
    public void deleteBonusTest() throws Exception {
        Bonus bonus = new Bonus(999, "BBB", 2000);
        boolean isOK = false;
        BonusDAO dao = new BonusDAO();
        List<Bonus> bonuses = dao.select();
        for (Bonus bonus1 : bonuses) {
            if (bonus1.getId() == bonus.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.delete(bonus);
        boolean exists = false;
        List<Bonus> bonusPo = dao.select();
        for (Bonus bonus1 : bonusPo) {
            if (bonus1.getId() == bonus.getId()) {
                exists = true;
            }
        }
        Assert.assertFalse(exists);
    }

    @Test
    public void insertJobsTest() throws Exception {
        Jobs job = new Jobs(999, "AAA", 1000);
        JobsDAO dao = new JobsDAO();
        List<Jobs> jobsList = dao.select();
        for (Jobs job1 : jobsList) {
            if (job1.getId() == job.getId()) {
                throw new Exception("Jest zle");
            }
        }
        dao.insert(job);
        boolean isOK = false;
        List<Jobs> jobsPoInsercie = dao.select();
        for (Jobs job1 : jobsPoInsercie) {
            if (job1.getId() == job.getId()) {
                isOK = true;
            }
        }
        Assert.assertTrue(isOK);
    }

    @Test
    public void updateJobsTest() throws Exception {
        Jobs job = new Jobs(999, "BBB", 2000);
        boolean isOK = false;
        JobsDAO dao = new JobsDAO();
        List<Jobs> jobsList = dao.select();
        for (Jobs job1 : jobsList) {
            if (job1.getId() == job.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.update(job);
        boolean exists = false;
        List<Jobs> jobsPoUpdate = dao.select();
        for (Jobs job1 : jobsPoUpdate) {
            if (job1.getId() == job.getId() && job1.getJobname().equals(job.getJobname())
                    && job1.getMinsalary() == job.getMinsalary()) {
                exists = true;
                break;
            }
        }
        Assert.assertTrue(exists);
    }

    @Test
    public void deleteJobsTest() throws Exception {
        Jobs job = new Jobs(999, "BBB", 2000);
        boolean isOK = false;
        JobsDAO dao = new JobsDAO();
        List<Jobs> jobsList = dao.select();
        for (Jobs job1 : jobsList) {
            if (job1.getId() == job.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.delete(job);
        boolean exists = false;
        List<Jobs> jobsPo = dao.select();
        for (Jobs job1 : jobsPo) {
            if (job1.getId() == job.getId()) {
                exists = true;
            }
        }
        Assert.assertFalse(exists);
    }

    @Test
    public void insertPostTest() throws Exception {
        Post post = new Post(999, "AAA", 9);
        PostDAO dao = new PostDAO();
        List<Post> postList = dao.select();
        for (Post post1 : postList) {
            if (post1.getId() == post.getId()) {
                throw new Exception("Jest zle");
            }
        }
        dao.insert(post);
        boolean isOK = false;
        List<Post> postPoInsercie = dao.select();
        for (Post post1 : postPoInsercie) {
            if (post1.getId() == post.getId()) {
                isOK = true;
            }
        }
        Assert.assertTrue(isOK);
    }

    @Test
    public void updatePostTest() throws Exception {
        Post post = new Post(999, "BBB", 8);
        boolean isOK = false;
        PostDAO dao = new PostDAO();
        List<Post> postList = dao.select();
        for (Post post1 : postList) {
            if (post1.getId() == post.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.update(post);
        boolean exists = false;
        List<Post> postPoUpdate = dao.select();
        for (Post post1 : postPoUpdate) {
            if (post1.getId() == post.getId() && post1.getPostname().equals(post.getPostname())
                    && post1.getPaymentcategory() == post.getPaymentcategory()) {
                exists = true;
                break;
            }
        }
        Assert.assertTrue(exists);
    }

    @Test
    public void deletePostTest() throws Exception {
        Post post = new Post(999, "BBB", 8);
        boolean isOK = false;
        PostDAO dao = new PostDAO();
        List<Post> postList = dao.select();
        for (Post post1 : postList) {
            if (post1.getId() == post.getId()) {
                isOK = true;
                break;
            }
        }
        Assert.assertTrue(isOK);

        dao.delete(post);
        boolean exists = false;
        List<Post> postPo = dao.select();
        for (Post post1 : postPo) {
            if (post1.getId() == post.getId()) {
                exists = true;
            }
        }
        Assert.assertFalse(exists);
    }
}
